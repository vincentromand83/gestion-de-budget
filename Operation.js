"use strict";
exports.__esModule = true;
exports.Operation = void 0;
var Operation = /** @class */ (function () {
    function Operation(montant, categorie, titre) {
        this.date = new Date();
        this.montant = 0;
        this.categorie = 'Maison' || 'Voiture' || 'Loisirs' || 'Alimentaire' || '';
        this.titre = "";
        this.montant = montant;
        this.categorie = categorie;
        this.titre = titre;
    }
    Operation.prototype.getDate = function () {
        return this.date;
    };
    Operation.prototype.setDate = function (date) {
        this.date = date;
    };
    Operation.prototype.getMontant = function () {
        return this.montant;
    };
    Operation.prototype.setMontant = function (montant) {
        this.montant = montant;
    };
    Operation.prototype.getCategorie = function () {
        return this.categorie;
    };
    Operation.prototype.setCategorie = function (categorie) {
        this.categorie = categorie;
    };
    Operation.prototype.getTitre = function () {
        return this.titre;
    };
    Operation.prototype.setTitre = function (titre) {
        this.titre = titre;
    };
    Operation.prototype.creerCategories = function () {
    };
    return Operation;
}());
exports.Operation = Operation;
