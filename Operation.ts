export class Operation
 {
    private date: Date = new Date();
    private montant: number = 0;
    private categorie: string = 'Maison' || 'Voiture' || 'Loisirs' || 'Alimentaire' || '';
    private titre: string = "";

    constructor(montant: number, categorie: string, titre: string) {
        this.montant = montant;
        this.categorie = categorie;
        this.titre = titre;
    }

    getDate(){
        return this.date
    }

    setDate(date:Date){
        this.date = date
    }

    getMontant(){
        return this.montant
    }

    setMontant(montant: number){
        this.montant = montant
    }

    getCategorie(){
        return this.categorie
    }

    setCategorie(categorie: string){
        this.categorie = categorie
    }

    getTitre(){
        return this.titre
    }

    setTitre(titre: string){
        this.titre = titre
    }

    creerCategories() {

    }
}