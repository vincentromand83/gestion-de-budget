"use strict";
exports.__esModule = true;
exports.Compte = void 0;
var Compte = /** @class */ (function () {
    function Compte(nomProprietaire) {
        this.operations = [];
        this.nomProprietaire = nomProprietaire;
    }
    Compte.prototype.getNomProprietaire = function () {
        return this.nomProprietaire;
    };
    Compte.prototype.setNomProprietaire = function (nomProprietaire) {
        this.nomProprietaire = nomProprietaire;
    };
    Compte.prototype.calculerSolde = function () {
        var solde = 0;
        for (var i = 0; i < this.operations.length; i++) {
            console.log("ceci est ma longueur : " + this.operations.length);
            solde = solde + this.operations[i].getMontant();
            console.log("Fonction : " + solde);
        }
        return solde;
    };
    Compte.prototype.calculerTotalRecettes = function () {
        var totalRecettes = 0;
        for (var i = 0; i < this.operations.length; i++) {
            if (this.operations[i].getMontant() > 0) {
                totalRecettes = totalRecettes + this.operations[i].getMontant();
            }
        }
        return totalRecettes;
    };
    Compte.prototype.calculerTotalDepenses = function () {
        var totalDepenses = 0;
        for (var i = 0; i < this.operations.length; i++) {
            if (this.operations[i].getMontant() > 0) {
                totalDepenses = totalDepenses + this.operations[i].getMontant();
            }
        }
        return totalDepenses;
    };
    Compte.prototype.afficherHTML = function () {
    };
    Compte.prototype.ajouterOperation = function (ope) {
        this.operations.push(ope);
    };
    return Compte;
}());
exports.Compte = Compte;
