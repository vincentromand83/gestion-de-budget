import { Operation } from "./Operation";

export class Compte {
    private nomProprietaire: string;
    private operations: Operation[] = [];

    constructor(nomProprietaire: string) {
        this.nomProprietaire = nomProprietaire;
    }

    getNomProprietaire(){
        return this.nomProprietaire
    }

    setNomProprietaire(nomProprietaire: string){
        this.nomProprietaire = nomProprietaire;
    }

    calculerSolde(){
        let solde: number = 0
        for(let i = 0; i < this.operations.length; i++){
            console.log("ceci est ma longueur : " + this.operations.length);
            solde = solde + this.operations[i].getMontant()
            console.log("Fonction : " + solde)
        }
        return solde
    }

    calculerTotalRecettes(){
        let totalRecettes: number = 0
        
        for(let i = 0; i < this.operations.length; i++){
            if(this.operations[i].getMontant() > 0){
                totalRecettes = totalRecettes + this.operations[i].getMontant()
            }
        }

        return totalRecettes
    }

    calculerTotalDepenses(){
        let totalDepenses: number = 0
        
        for(let i = 0; i < this.operations.length; i++){
            if(this.operations[i].getMontant() > 0){
                totalDepenses = totalDepenses + this.operations[i].getMontant()
            }
        }

        return totalDepenses
    }

    afficherHTML(){

    }

    ajouterOperation(ope:Operation){
        this.operations.push(ope);
    }
}